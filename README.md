# g1-monetary-licence-packager

Publish up to date Ǧ1 monetary licence to various software ecosystem for easyer integration.

## Pseudo-code role :
- import https://git.duniter.org/documents/licence-g1
- if no new version, stop here
- for each software ecosystem with a maintainer to handle package manager publication :
  - package and publish monetary licence files to the target package manager

